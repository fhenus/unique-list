
export interface UniqueListId<TValue> {
    (value: TValue): string | number;
}

export class UniqueList<TValue> {

    private hash = {};
    private list = new Array<TValue>();
    public length: number;

    constructor(private uniqueCallBack: UniqueListId<TValue>, initialValues?: TValue[]) {

        if (initialValues) {

            this.pushAll(initialValues);
        }
    }

    hasItem(id: string | number): boolean {

        return this.hash[id] !== undefined;
    }

    get(id: string | number): TValue {

        return this.hash[id];
    }

    getAllKeys(): {} {

        var keys = new Array<any>();

        for (var prop in this.hash) {

            keys.push(prop);
        }

        return keys;
    }

    getAll(): TValue[] {

        return this.list;
    }

    getAt(idx: number): TValue {

        return this.list[idx];
    }

    setAt(idx: number, item: TValue): void {

        this.list[idx] = item;
        this.hash[this.uniqueCallBack(item)] = item;
    }

    hasItems(): boolean {

        return this.list.length > 0;
    }

    push(item: TValue): void {

        var uniqueId = this.uniqueCallBack(item);

        if (!this.hash[uniqueId]) {

            this.hash[uniqueId] = item;
            this.list.push(item);
            this.length = this.list.length;
        }
    }

    pushAll(items: TValue[]): void {

        items.forEach(value => {

            this.push(value);
        })
    }

    insertAt(item: TValue, idx: number): void {

        var uniqueId = this.uniqueCallBack(item);

        if (!this.hash[uniqueId]) {

            this.hash[uniqueId] = item;
            this.list.splice(idx, 0, item);
            this.length = this.list.length;
        }
    }

    remove(item: TValue) {

        var uniqueId = this.uniqueCallBack(item);

        if (this.hash[uniqueId]) {

            var idx = this.list.indexOf(item);

            this.list.splice(idx, 1);
            this.length = this.list.length;
            delete this.hash[uniqueId];
        }
    }

    clear() {

        this.list = [];
        this.hash = {};
        this.length = 0;
    }
}
